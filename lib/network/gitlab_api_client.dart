import 'dart:convert';

import 'package:cost_of_care/bloc/download_cdm_bloc/download_cdm_progress/download_file_button_event.dart';
import 'package:cost_of_care/models/download_cdm_model.dart';
import 'package:cost_of_care/util/api_config.dart';
import 'package:dio/dio.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter/foundation.dart';

import '../main.dart';
import '../models/outpatient_procedure.dart';
import '../util/api_config.dart';

class GitLabApiClient {
  Dio dio;

  GitLabApiClient(this.dio);

  Future fetchStatesName() async {
    List<String> states = [];
    ApiConfig apiConfig = new ApiConfig();
    String url = apiConfig.gitlabApiFetchList + "&per_page=100";
    var response;

    try {
      response = await dio.get(url);
    } catch (e) {
      if (e is DioError) {
        if (DioErrorType.receiveTimeout == e.type ||
            DioErrorType.connectTimeout == e.type) {
          throw Exception(
              "Please check your internet connection and try again");
        } else if (DioErrorType.other == e.type) {
          if (e.message.contains('SocketException')) {
            throw Exception('No Internet Connection');
          }
        } else {
          throw Exception(
              "Problem connecting to the server. Please try again.");
        }
      }
      throw Exception("Problem connecting to the server. Please try again.");
    }

    List<dynamic> responseBody = response.data;
    if (responseBody.length == 0 || response.statusCode != 200) {
      throw Exception("Problem Connecting to Server, Try Again Later");
    }

    for (int i = 0; i < responseBody.length; i++) {
      Map<String, dynamic> currentHospital = responseBody[i];
      String stateName = currentHospital['name'];
      stateName = stateName.substring(0, stateName.length);
      states.add(stateName);
    }
    return states;
  }

  Future getAvailableCdm(String stateName) async {
    ApiConfig apiConfig = new ApiConfig();
    String url = apiConfig.CDMListHospital + stateName + "/address.json";
    int i = 1;

    var response;
    try {
      response = await dio.get(url);
    } catch (e) {
      if (response == null) {
        throw Exception("CDMs Not Available For Your Location");
      }
      if (e is DioError) {
        if (DioErrorType.receiveTimeout == e.type ||
            DioErrorType.connectTimeout == e.type) {
          throw Exception(
              "Please check your internet connection and try again");
        } else if (DioErrorType.other == e.type) {
          if (e.message.contains('SocketException')) {
            throw Exception('No Internet Connection');
          }
        } else {
          throw Exception(
              "Problem connecting to the server. Please try again.");
        }
      }
      throw Exception("Problem connecting to the server. Please try again.");
    }

    // var headers = response.headers;

    List<dynamic> responseBody = json.decode(response.data);
    debugPrint('kkp' + response.data.toString());

    if (responseBody.length == 0) {
      throw Exception("CDMs Not Available For Your Location");
    }

    return responseBody;
  }

  Future<List<DownloadCdmModel>> parseAvailableCdm(
      List<dynamic> elements) async {
    List<DownloadCdmModel> name = [];
    for (int i = 0; i < elements.length; i++) {
      Map<String, dynamic> currentHospital = elements[i];
      String hospitalName = currentHospital['Hospital'];
      String hospitalAddress = currentHospital['Address'];
      debugPrint('hospitalname' + hospitalName);
      debugPrint('hospitaladdress' + currentHospital['Address']);
      // hospitalName = hospitalName.substring(0, hospitalName.length - 4);
      name.add(DownloadCdmModel(hospitalName, 0, 0, hospitalAddress));
    }
    return name;
  }

  Future getCSVFileSize(DownloadFileButtonClick event) async {
    String url = ApiConfig().gitlabApiGetCDMFileSize +
        "%2F${event.stateName}%2F${event.hospitalName}" +
        ".csv" +
        "?ref=master";

    try {
      var response = await dio.head(url);
      Headers map = response.headers;
      double total = double.parse(map.value('x-gitlab-size'));
      return total;
    } catch (e) {
      if (e is DioError) {
        if (DioErrorType.receiveTimeout == e.type ||
            DioErrorType.connectTimeout == e.type) {
          throw Exception(
              "Please check your internet connection and try again");
        } else if (DioErrorType.other == e.type) {
          if (e.message.contains('SocketException')) {
            throw Exception('No Internet Connection');
          }
        } else {
          throw Exception(
              "Problem connecting to the server. Please try again.");
        }
      }
      throw Exception("Problem connecting to the server. Please try again.");
    }
  }

  Future downloadCSVFile(DownloadFileButtonClick event) async {
    String url = ApiConfig().downloadCDMApi +
        "/${event.stateName}/${event.hospitalName}" +
        ".csv";
    //Saving stateName for refreshing
    box.put(event.hospitalName, event.stateName);
    Stream<FileResponse> fileStream;
    //FileStream is handled properly by stream builder. So, testing is not required
    fileStream = DefaultCacheManager().getFileStream(url, withProgress: true);
    return fileStream;
  }

  Future getInpatientProcedures() async {
    List<OutpatientProcedure> inpatientProcedures = [];
    String url = ApiConfig().inpatientProcedureApi;
    var response;
    try {
      response = await dio.get(url);
    } catch (e) {
      if (e is DioError) {
        if (DioErrorType.receiveTimeout == e.type ||
            DioErrorType.connectTimeout == e.type) {
          throw Exception(
              "Please check your internet connection and try again");
        } else if (DioErrorType.other == e.type) {
          if (e.message.contains('SocketException')) {
            throw Exception('No Internet Connection');
          }
        } else {
          throw Exception(
              "Problem connecting to the server. Please try again.");
        }
      }
      throw Exception("Problem connecting to the server. Please try again.");
    }

    var elements = jsonDecode(response.data) as List;
    elements.forEach((element) {
      inpatientProcedures.add(OutpatientProcedure.fromJson(element));
    });
    return inpatientProcedures;
  }

  Future getOutpatientProcedures() async {
    List<OutpatientProcedure> outpatientProcedures = [];
    String url = ApiConfig().outpatientProcedureApi;
    var response;
    try {
      response = await dio.get(url);
    } catch (e) {
      if (e is DioError) {
        if (DioErrorType.receiveTimeout == e.type ||
            DioErrorType.connectTimeout == e.type) {
          throw Exception(
              "Please check your internet connection and try again");
        } else if (DioErrorType.other == e.type) {
          if (e.message.contains('SocketException')) {
            throw Exception('No Internet Connection');
          }
        } else {
          throw Exception(
              "Problem connecting to the server. Please try again.");
        }
      }
      throw Exception("Problem connecting to the server. Please try again.");
    }

    var elements = jsonDecode(response.data) as List;
    elements.forEach((element) {
      outpatientProcedures.add(OutpatientProcedure.fromJson(element));
    });
    return outpatientProcedures;
  }
}
