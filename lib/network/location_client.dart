import 'dart:async';
import 'package:cost_of_care/models/user_location_data.dart';
import 'package:cost_of_care/util/states_abbreviation.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter/foundation.dart';

class LocationClient {
  Position position;
  String state;
  String address;
  LocationClient();
  Future<void> execute() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
  }

  Future<UserLocationData> getCurrentLocation() async {
    await execute();
    bool serviceStatus =
        await Permission.locationWhenInUse.serviceStatus.isEnabled;
    if (serviceStatus == false) {
      throw Exception("You need to Enable GPS/Location Service");
    }
    try {
      position = await Geolocator.getCurrentPosition();

      if (position == null) {
        debugPrint("this is location debug point");
        throw Exception("Location Not Found");
      }
    } catch (e) {
      throw Exception("Location Not Found");
    }

    try {
      List<Placemark> placeMark = await placemarkFromCoordinates(
          position.latitude, position.longitude,
          localeIdentifier: "en");
      address = placeMark[0].name +
          ", " +
          placeMark[0].locality +
          ", " +
          placeMark[0].administrativeArea +
          ", " +
          placeMark[0].country;
      debugPrint("My Address" + address);
      //Save address
      StatesAbbreviation statesAbbreviation = new StatesAbbreviation();
      if (placeMark[0].administrativeArea.length == 2 &&
          statesAbbreviation.containsState(placeMark[0].administrativeArea)) {
        state =
            statesAbbreviation.getStateName(placeMark[0].administrativeArea);
      } else {
        state = placeMark[0].administrativeArea;
      }

      return UserLocationData(state, position.latitude,
          position.longitude, address);
    } catch (e) {
      throw Exception("Location Not Found");
    }
  }
}
