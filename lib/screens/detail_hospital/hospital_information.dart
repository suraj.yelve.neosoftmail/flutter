import 'package:cached_network_image/cached_network_image.dart';
import 'package:cost_of_care/bloc/detail_hospital_bloc/detail_hospital_list_bloc.dart';
import 'package:cost_of_care/models/hospitals.dart';
import 'package:cost_of_care/repository/compare_screen_repository_impl.dart';
import 'package:cost_of_care/repository/nearby_hospital_repository_impl.dart';

import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import 'hospital_detail_body.dart';

// ignore: must_be_immutable
class HospitalDetail extends StatefulWidget {
  HospitalDetail(this.hospital);

  Hospitals hospital;

  @override
  State<HospitalDetail> createState() => _HospitalDetailState();
}

class _HospitalDetailState extends State<HospitalDetail> {
  NearbyHospitalsRepoImpl nearbyHospitalsRepository =
      new NearbyHospitalsRepoImpl();

  Future imageFuture;

  DetailHospitalListBloc detailHospitalListBloc;

  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.amber[600],
            centerTitle: true,
            title: Text(
              "Hospital's Details",
              style: TextStyle(
                  fontFamily: "Source",
                  fontSize: 22,
                  fontWeight: FontWeight.bold),
            )),
        body: ListView(
          children: [
            Padding(
              padding: EdgeInsets.all(8),
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 200,
                    decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(40)),
                    child: FutureBuilder(
                      future: imageFuture,
                      builder: (BuildContext context,
                          AsyncSnapshot<dynamic> snapshot) {
                        if (snapshot.hasError) {
                          return Container(
                            margin: EdgeInsets.only(
                                left: 10, top: 16, bottom: 16, right: 10),
                            width: 100,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                            ),
                            child: new FittedBox(
                                fit: BoxFit.fill,
                                child: Image.asset('assets/placeholder.png')),
                          );
                        } else if (snapshot.hasData) {
                          return CachedNetworkImage(
                            imageUrl: snapshot.data,
                            imageBuilder: (context, imageProvider) => Container(
                              width: 100,
                              decoration: BoxDecoration(
                                // border: Border.all(color: Colors.black),
                                borderRadius: BorderRadius.circular(40),
                                image: DecorationImage(
                                  image: imageProvider,
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            placeholder: (context, url) => Container(
                                margin: EdgeInsets.only(
                                    left: 10, top: 16, bottom: 16, right: 10),
                                width: 100,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                ),
                                child: Center(
                                  child: CircularProgressIndicator(),
                                )),
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error),
                          );
                        }
                        return Container(
                            width: 100,
                            child: Shimmer.fromColors(
                              baseColor: Colors.grey[300],
                              highlightColor: Colors.white,
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.grey[300],
                                  borderRadius: BorderRadius.circular(40),
                                ),
                              ),
                            ));
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  BodyDetail(
                      detailHospitalListBloc, widget.hospital),
                 
                ],
              ),
            )
          ],
        ));
  }

  @override
  void initState() {
    super.initState();
    imageFuture = nearbyHospitalsRepository.fetchImages(widget.hospital.name);

    detailHospitalListBloc =
        new DetailHospitalListBloc(CompareScreenRepositoryImpl());
  }
}
