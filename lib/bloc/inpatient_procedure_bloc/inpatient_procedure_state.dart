part of 'inpatient_procedure_bloc.dart';

abstract class InpatientProcedureState extends Equatable {
  const InpatientProcedureState();
}

class InpatientProcedureInitial extends InpatientProcedureState {
  @override
  List<Object> get props => [];
}

class InpatientProcedureLoadingState extends InpatientProcedureState {
  @override
  List<Object> get props => [];
}

class InpatientProcedureLoadedState extends InpatientProcedureState {
  final List<OutpatientProcedure> inpatientProcedures;

  InpatientProcedureLoadedState(this.inpatientProcedures);
  @override
  List<Object> get props => [inpatientProcedures];
}

class InpatientProcedureErrorState extends InpatientProcedureState {
  final String message;

  InpatientProcedureErrorState(this.message);

  @override
  List<Object> get props => [message];
}
